# Rename a newick tree
### Use case
You have a newick tree where the leaf names require renaming
### Usage
 1. Make a tab separated file which has the format 
    ```
    old name 1\tnewname 1
    old name 1\tnewname 2
    ....
    ```
    Save with a sensible name e.g names.tsv

2. Run the command as follows
   ```
   rename_tree.py -t /path/to/input/tree.nwk  -n /path/to/input/names.tsv
   ```

Full usage is
```
usage: rename_tree.py [-h] -t /path/to/input/tree.nwk -n
                      /path/to/input/names.tsv
                      [-o /path/to/input/renamed_tree.nwk]

Rename Tree file

optional arguments:
  -h, --help            show this help message and exit
  -t /path/to/input/tree.nwk, --tree_file /path/to/input/tree.nwk
                        Newick tree file
  -n /path/to/input/names.tsv, --name_file /path/to/input/names.tsv
                        A tab separated file of names: old_name new_name
  -o /path/to/input/renamed_tree.nwk, --output_file /path/to/input/renamed_tree.nwk
                        Path to output renamed tree file (optional)
```