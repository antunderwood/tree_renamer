#!/usr/bin/env python3
import os
import argparse
import re
import csv

def make_name_dict_from_name_tsv(name_file):
    """
    make a dictionary old_name : new_name from the names in a tsv file
    """
    name_dict = {}
    with open(name_file) as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
            name_dict[row[0]] = row[1]
    return name_dict

def read_in_tree_as_string(tree_file):
    """
    get tree file  as a string
    """
    with open(tree_file) as tree:
        tree_string = tree.read()
    return tree_string

def replace_names(name_dict, tree_string):
    """
    replace names in tree file string and return a new string with replacements
    """
    new_tree_string = tree_string
    for old_name, new_name in name_dict.items():
        if '{0}:'.format(old_name) not in new_tree_string:
            exit('"{0}" is not found in the tree leaf names'. format(old_name))
        new_tree_string = re.sub('{0}:'.format(old_name), '{0}:'.format(new_name), new_tree_string)
    return new_tree_string



def parse_args():
    """
    parse CLI arguments
    """
    parser = argparse.ArgumentParser(description="Rename Tree file")
    parser.add_argument('-t',
                        '--tree_file',
                        metavar='/path/to/input/tree.nwk',
                        help='Newick tree file', required=True)
    parser.add_argument('-n',
                    '--name_file',
                    metavar='/path/to/input/names.tsv',
                    help='A tab separated file of names: old_name\tnew_name', required=True)
    parser.add_argument('-o',
                    '--output_file',
                    metavar='/path/to/input/renamed_tree.nwk',
                    help='Path to output renamed tree file (optional)')

    return parser.parse_args()

def main():
    args = parse_args()

    # make output filename
    if args.output_file:
        output_tree_path = args.output_file
    else:
        output_tree_path = '{0}.renamed{1}'.format (os.path.splitext(args.tree_file)[0], os.path.splitext(args.tree_file)[1])

    with open(output_tree_path, 'w') as outfile:
        outfile.write(replace_names
                        (
                            make_name_dict_from_name_tsv(args.name_file),
                            read_in_tree_as_string(args.tree_file)
                        )
                    )
    

if __name__ == "__main__":
    main()
